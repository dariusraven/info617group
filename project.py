import pandas as pd
from transformers import DistilBertTokenizerFast, DistilBertForSequenceClassification, Trainer, TrainingArguments
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset, DataLoader

import torch

if torch.cuda.is_available():
    print(f"CUDA is available. GPU: {torch.cuda.get_device_name(0)}")
else:
    print("CUDA is not available. Training on CPU.")
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


# Load the dataset
df = pd.read_excel('C:/VCU/INFO617/info617group/INFO_617_Group_Project_Train_Val.xlsx', engine='openpyxl')

# Convert category labels to numbers
def category_to_number(category):
    category_map = {
        "TREAT": 1,
        "EXPLAIN": 2,
        "DIAGNOISE": 3,
        "QUES": 4,
        "REFERRAL": 5,
        "WAIT": 6,
        "THANK": 7,
        "GREET": 8,
        "RECEIVE": 9,
        "REPEAT": 10,
        "REMIND": 11,
        "CONSOLE": 12,
        "WISH": 13,
        "REQUEST_INFORMATION": 14,
        "FUTURE_SUPPORT": 15,
    }
    return category_map.get(category, None)

df['label'] = df['Label'].apply(category_to_number)

# Tokenization
tokenizer = DistilBertTokenizerFast.from_pretrained('distilbert-base-uncased')

class TextDataset(Dataset):
    def __init__(self, texts, labels):
        self.tokenizer = tokenizer
        self.texts = texts
        self.labels = labels

    def __len__(self):
        return len(self.texts)

    def __getitem__(self, idx):
        text = self.texts[idx]
        labels = self.labels[idx]
        tokenized = self.tokenizer(text, padding='max_length', truncation=True, max_length=512, return_tensors="pt")
        input_ids = tokenized['input_ids'].squeeze(0)
        attention_mask = tokenized['attention_mask'].squeeze(0)
        return {'input_ids': input_ids, 'attention_mask': attention_mask, 'labels': labels}

# Splitting dataset
X_train, X_test, y_train, y_test = train_test_split(df['Sentence'], df['label'], test_size=0.2, random_state=54)

train_dataset = TextDataset(X_train.tolist(), y_train.tolist())
test_dataset = TextDataset(X_test.tolist(), y_test.tolist())

from transformers import DistilBertForSequenceClassification, Trainer, TrainingArguments
from transformers import TrainerCallback
import numpy as np
import optuna

# Define your model
model = DistilBertForSequenceClassification.from_pretrained('distilbert-base-uncased', num_labels=16)
model.to(device)

# Define a method to return model initialization function
def model_init():
    return DistilBertForSequenceClassification.from_pretrained('distilbert-base-uncased', num_labels=16)

# Define TrainingArguments
training_args = TrainingArguments(
    output_dir='./results',
    num_train_epochs=3,  # This can be tuned during the hyperparameter search if desired
    per_device_train_batch_size=8,  # Consider tuning batch size and learning rate
    per_device_eval_batch_size=8,
    warmup_steps=500,  # And any other hyperparameters you're interested in
    weight_decay=0.01,
    logging_dir='./logs',
    evaluation_strategy="epoch",  # Evaluate at the end of each epoch
    report_to="none",  # Avoids logging to any online services
)

# Initialize the Trainer
trainer = Trainer(
    model_init=model_init,
    args=training_args,
    train_dataset=train_dataset,
    eval_dataset=test_dataset,
    # You can also include compute_metrics function here if you have one defined
)

# Define the hyperparameter search space using Optuna
def my_hp_space(trial):
    return {
        "learning_rate": trial.suggest_float("learning_rate", 1e-5, 5e-5, log=True),
        "num_train_epochs": trial.suggest_int("num_train_epochs", 1, 5),
        "per_device_train_batch_size": trial.suggest_categorical("per_device_train_batch_size", [8, 16, 32]),
    }

# Run hyperparameter search using Optuna
best_trial = trainer.hyperparameter_search(direction="minimize",  # or "maximize" depending on your objective
                                           hp_space=my_hp_space,
                                           backend="optuna",
                                           n_trials=10  # Number of trials; adjust based on computational budget
                                          )

# Access the best model and its arguments
print(best_trial)

print(best_trial.hyperparameters)

# First, create a copy of the training_args as a dictionary
best_training_args_dict = training_args.to_dict()

# Update this dictionary with the best hyperparameters
best_training_args_dict.update(best_trial.hyperparameters)

# Now, explicitly set 'output_dir' to ensure there is no conflict
best_training_args_dict['output_dir'] = './results_best'

# Use the updated dictionary to create the TrainingArguments instance
best_training_args = TrainingArguments(**best_training_args_dict)

from sklearn.metrics import accuracy_score
from transformers import EvalPrediction


def compute_metrics(p: EvalPrediction):
    preds = np.argmax(p.predictions, axis=1)
    return {"accuracy": accuracy_score(p.label_ids, preds)}


# Then you can proceed to re-instantiate the Trainer and re-train the model as before
best_trainer = Trainer(
    model_init=model_init,
    args=best_training_args,
    train_dataset=train_dataset,
    eval_dataset=test_dataset,
    compute_metrics=compute_metrics,  # Uncomment if you have defined a metrics function
)

# Continue with your training and evaluation as planned


# Train with the best hyperparameters
best_trainer.train()


# Evaluate the model
eval_results = best_trainer.evaluate()

# Print out the accuracy
print(f"Model Accuracy: {eval_results['eval_accuracy']}")



# Specify the directory where you want to save the model and tokenizer
save_directory = "./model_save_best"

# Save the model using the save_model method of the Trainer
best_trainer.save_model(save_directory)

# Assuming you have a tokenizer used with this model, save it as well
tokenizer.save_pretrained(save_directory)