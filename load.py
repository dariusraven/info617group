# -*- coding: utf-8 -*-
"""
Created on Sat Mar 30 15:36:33 2024

@author: Jordan
"""

from transformers import DistilBertForSequenceClassification, DistilBertTokenizerFast
import torch
# model was saved in directory final_model
model_directory = "./final_model"

# Load the trained model
model = DistilBertForSequenceClassification.from_pretrained(model_directory)

# Load the tokenizer
tokenizer = DistilBertTokenizerFast.from_pretrained(model_directory)




def predict(text, model, tokenizer):
    # Tokenize the input text
    inputs = tokenizer(text, return_tensors="pt", padding=True, truncation=True, max_length=512)

    # Move model to evaluation mode
    model.eval()

    # Perform prediction
    with torch.no_grad():
        outputs = model(**inputs)

    # Extract logits
    logits = outputs.logits

    # Convert to probabilities
    probs = torch.nn.functional.softmax(logits, dim=-1)

    predicted_class = probs.argmax(dim=-1).item()

    return predicted_class


import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset, DataLoader
import torch



# Load the validation set
df = pd.read_excel('C:/VCU/INFO617/info617group/INFO_617_Group_Project_Train_Val.xlsx', engine='openpyxl')

#begin data preperation for consumption by model
def category_to_number(category):
    category_map = {
        "TREAT": 1,
        "EXPLAIN": 2,
        "DIAGNOISE": 3,
        "QUES": 4,
        "REFERRAL": 5,
        "WAIT": 6,
        "THANK": 7,
        "GREET": 8,
        "RECEIVE": 9,
        "REPEAT": 10,
        "REMIND": 11,
        "CONSOLE": 12,
        "WISH": 13,
        "REQUEST_INFORMATION": 14,
        "FUTURE_SUPPORT": 15,
    }
    return category_map.get(category, None)

df['label'] = df['Label'].apply(category_to_number)



class TextDataset(Dataset):
    def __init__(self, texts, labels):
        self.tokenizer = tokenizer
        self.texts = texts
        self.labels = labels

    def __len__(self):
        return len(self.texts)

    def __getitem__(self, idx):
        text = self.texts[idx]
        labels = self.labels[idx]
        tokenized = self.tokenizer(text, padding='max_length', truncation=True, max_length=512, return_tensors="pt")
        input_ids = tokenized['input_ids'].squeeze(0)
        attention_mask = tokenized['attention_mask'].squeeze(0)
        return {'input_ids': input_ids, 'attention_mask': attention_mask, 'labels': labels}



eval_dataset = TextDataset(df['Sentence'],df['Label'])
# Create DataLoader 
eval_loader = DataLoader(eval_dataset, batch_size=8)



# Model Evaluation

from sklearn.metrics import f1_score


def evaluate(model, dataloader):
    model.eval()
    predictions, true_labels = [], []
    
    with torch.no_grad():
        for batch in dataloader:
            inputs = {'input_ids': batch['input_ids'], 'attention_mask': batch['attention_mask']}
            outputs = model(**inputs)
            logits = outputs.logits
            preds = torch.argmax(logits, dim=-1)
            predictions.extend(preds.tolist())
            true_labels.extend(batch['labels'])

    # calculate F1 score
    f1 = f1_score(true_labels, predictions, average='macro')
    return f1

# Evaluate the model against loaded validation set
f1 = evaluate(model, eval_loader)
print(f"Model F1 Score: {f1}")
