# -*- coding: utf-8 -*-
"""
Created on Tue Apr  9 23:02:28 2024

@author: Jordan
"""

import pandas as pd
from transformers import DistilBertTokenizerFast, DistilBertForSequenceClassification, Trainer, TrainingArguments
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset, DataLoader

import torch

if torch.cuda.is_available():
    print(f"CUDA is available. GPU: {torch.cuda.get_device_name(0)}")
else:
    print("CUDA is not available. Training on CPU.")
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


# Load the dataset
df = pd.read_excel('C:/VCU/INFO617/info617group/INFO_617_Group_Project_Train_Val.xlsx', engine='openpyxl')

# Convert category labels to numbers
def category_to_number(category):
    category_map = {
        "TREAT": 1,
        "EXPLAIN": 2,
        "DIAGNOISE": 3,
        "QUES": 4,
        "REFERRAL": 5,
        "WAIT": 6,
        "THANK": 7,
        "GREET": 8,
        "RECEIVE": 9,
        "REPEAT": 10,
        "REMIND": 11,
        "CONSOLE": 12,
        "WISH": 13,
        "REQUEST_INFORMATION": 14,
        "FUTURE_SUPPORT": 15,
    }
    return category_map.get(category, None)

df['label'] = df['Label'].apply(category_to_number)

# Tokenization
tokenizer = DistilBertTokenizerFast.from_pretrained('distilbert-base-uncased')

class TextDataset(Dataset):
    def __init__(self, texts, labels):
        self.tokenizer = tokenizer
        self.texts = texts
        self.labels = labels

    def __len__(self):
        return len(self.texts)

    def __getitem__(self, idx):
        text = self.texts[idx]
        labels = self.labels[idx]
        tokenized = self.tokenizer(text, padding='max_length', truncation=True, max_length=512, return_tensors="pt")
        input_ids = tokenized['input_ids'].squeeze(0)
        attention_mask = tokenized['attention_mask'].squeeze(0)
        return {'input_ids': input_ids, 'attention_mask': attention_mask, 'labels': labels}

# Splitting dataset
X_train, X_test, y_train, y_test = train_test_split(df['Sentence'], df['label'], test_size=0.2, random_state=54)

train_dataset = TextDataset(X_train.tolist(), y_train.tolist())
test_dataset = TextDataset(X_test.tolist(), y_test.tolist())

from transformers import DistilBertForSequenceClassification, Trainer, TrainingArguments
from transformers import TrainerCallback
import numpy as np
import optuna

# Define your model
model = DistilBertForSequenceClassification.from_pretrained('distilbert-base-uncased', num_labels=16)
model.to(device)

# Define a method to return model initialization function
def model_init():
    return DistilBertForSequenceClassification.from_pretrained('distilbert-base-uncased', num_labels=16)

# Define TrainingArguments
training_args = TrainingArguments(
     output_dir='./results',
     num_train_epochs=3,  # This can be tuned during the hyperparameter search if desired
     per_device_train_batch_size=8,  # Consider tuning batch size and learning rate
     per_device_eval_batch_size=8,
     warmup_steps=500,  # And any other hyperparameters you're interested in
     weight_decay=0.01,
     logging_dir='./logs',
     evaluation_strategy="epoch",  # Evaluate at the end of each epoch
     report_to="none",  # Avoids logging to any online services
)


best_hyperparameters = {'learning_rate': 4.331180533567719e-05, 'num_train_epochs': 2, 'per_device_train_batch_size': 8}
# First, create a copy of the training_args as a dictionary
best_training_args_dict = training_args.to_dict()

# Update this dictionary with the best hyperparameters
best_training_args_dict.update(best_hyperparameters)

# Now, explicitly set 'output_dir' to ensure there is no conflict
best_training_args_dict['output_dir'] = './results_best'

# Use the updated dictionary to create the TrainingArguments instance
best_training_args = TrainingArguments(**best_training_args_dict)

from sklearn.metrics import accuracy_score
from transformers import EvalPrediction


from sklearn.metrics import f1_score

def compute_metrics(p: EvalPrediction):
    preds = np.argmax(p.predictions, axis=1)
    f1 = f1_score(p.label_ids, preds, average='macro')  # You can change 'macro' to 'micro' or 'weighted' if needed
    return {"f1": f1}

# Then, integrate this new metrics function into your training process
best_trainer = Trainer(
    model_init=model_init,
    args=best_training_args,
    train_dataset=train_dataset,
    eval_dataset=test_dataset,
    compute_metrics=compute_metrics,  # Now this function returns F1 score
)

# Train with the best hyperparameters
best_trainer.train()

# Evaluate the model
eval_results = best_trainer.evaluate()

# Print out the F1 score
print(f"Model F1 Score: {eval_results['eval_f1']}")


# Specify the directory where you want to save the model and tokenizer
save_directory = "./final_model"

# Save the model using the save_model method of the Trainer
best_trainer.save_model(save_directory)

# Assuming you have a tokenizer used with this model, save it as well
tokenizer.save_pretrained(save_directory)



#----------------------------------------------------------------------------------
#--------------------------Evaluation/Validation Code-----------------------------------------
#----------------------------------------------------------------------------------

from transformers import DistilBertForSequenceClassification, DistilBertTokenizerFast
import torch
# model was saved in directory final_model
model_directory = "./final_model"

# Load the trained model
model = DistilBertForSequenceClassification.from_pretrained(model_directory)

# Load the tokenizer
tokenizer = DistilBertTokenizerFast.from_pretrained(model_directory)

# Load the dataset
df = pd.read_excel('C:/VCU/INFO617/info617group/INFO617_GroupProjectTestSet.xlsx', engine='openpyxl')

df['label'] = df['Label'].apply(category_to_number)

validation_dataset = TextDataset(df['Sentence'], df['label'])
validation_loader = DataLoader(validation_dataset, batch_size=8)



from sklearn.metrics import accuracy_score, confusion_matrix, f1_score
import seaborn as sns
import matplotlib.pyplot as plt


def evaluate(model, dataloader):
    model.eval()
    predictions, true_labels = [], []
    
    with torch.no_grad():
        for batch in dataloader:
            inputs = {'input_ids': batch['input_ids'], 'attention_mask': batch['attention_mask']}
            outputs = model(**inputs)
            logits = outputs.logits
            preds = torch.argmax(logits, dim=-1)
            predictions.extend(preds.tolist())
            true_labels.extend(batch['labels'])

    # Calculate accuracy
    accuracy = accuracy_score(true_labels, predictions)
    print(f"Accuracy Score: {accuracy * 100:.2f}%")

    # Calculate F1 score
    f1 = f1_score(true_labels, predictions, average='macro')
    print(f"Model F1 Score: {f1}")

    # Generate confusion matrix
    cm = confusion_matrix(true_labels, predictions)
    sns.heatmap(cm, annot=True, fmt='d')
    plt.title('Confusion Matrix')
    plt.ylabel('Actual Labels')
    plt.xlabel('Predicted Labels')
    plt.show()

# Evaluate the model against loaded validation set
evaluate(model, validation_loader)

